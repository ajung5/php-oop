<?php

//class
class Animal {

   //properti atau variabel
   public $name;
   public $legs = 4;
   public $cold_blooded = "no";

   //method atau function
   public function __construct ($string) {
      $this->name = $string;
   }

}

?>