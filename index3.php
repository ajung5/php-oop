<?php

// class
class robot {

   //properti atau variabel
   public $suara = 'Dor';
   public $jenis = 'Autobot';

   // metode atau function
   public function bersuara() {
      // $this berfungsi untuk mengambil properti yang ada didalam class
      echo 'NALNSnsld: ' . $this->suara;
   }

}

// membuat object bernama $robot1, dengan memanggil class robot
$robot1 = new robot;

// menampilkan object $robot1 dengan properti2 nya 
echo 'Suara robot ' . $robot1->suara . ' dan berjenis ' . $robot1->jenis . '<br>';

$robot1->bersuara();

?>