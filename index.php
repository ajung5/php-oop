<?php
   require_once ("animal.php");
   require_once ("frog.php");
   require_once ("ape.php");

   // membuat object bernama $sheep, dengan memanggil class animal
   $sheep = new Animal("shaun");
   echo "Nama hewan = " . $sheep->name . "<br>"; // "shaun"
   echo "Jumlah kaki = " . $sheep->legs . "<br>"; // 4
   echo "Berdarah dingin = " . $sheep->cold_blooded . "<br>"; // "no"

   echo "<br>";

   // membuat object bernama $katak, dengan memanggil class frog
   $katak = new frog("Katak Alaska");
   echo "Nama hewan = " . $katak->name . "<br>"; // "shaun"
   echo "Jumlah kaki = " . $katak->legs . "<br>"; // 4
   echo "Berdarah dingin = " . $katak->cold_blooded . "<br>"; // "no"
   echo $katak->jump(). "<br>";

   echo "<br>";

   // membuat object bernama $kera, dengan memanggil class ape
   $kera = new ape("Kera Babon");
   echo "Nama hewan = " . $kera->name . "<br>"; // "shaun"
   echo "Jumlah kaki = " . $kera->legs . "<br>"; // 4
   echo "Berdarah dingin = " . $kera->cold_blooded . "<br>"; // "no"
   echo  $kera->yell();

?>